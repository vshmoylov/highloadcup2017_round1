#!/bin/bash
#docker build -t hlcup_taran .
docker run --name taran -p80:80 --rm -it --net host\
    -v `pwd`/data.zip:/tmp/data/data.zip \
    -v `pwd`/options.txt:/tmp/data/options.txt \
    hlcup_taran 