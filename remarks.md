# Entities
    user
    location
    visit

Need to handle following methods:
    
    * GET /<entity>/<id> - 200|404
        * GET /users/:id
        * GET /locations/:id
        * GET /visits/:id
    
    * GET /users/<id>/visits - 200|400|404
        {"visits": [ ... ]} ordered by visited_at ASC
        Parameters:
            fromDate - visited_at > fromDate
            toDate   - visited_at < toDate
            country
            toDistance - distance < toDistance
    
    * GET /locations/<id>/avg - 200|400|404
        {"avg": 0.12345}
        Parameters:
            fromDate - visited_at > fromDate
            toDate   - visited_at < toDate
            fromAge  - (current_timestamp-birth_date) > fromAge
            toAge    - (current_timestamp-birth_date) < toAge
            gender
    
    * POST /<entity>/<id> - 200|400|404
        Updates existing entity
    
    * POST /<entity>/new  - 200|400
        Creates new entity
    
# Tables
    t_users { //    alias: u
        1: id           [uint32]        (unique),
        2: email        [nvarchar(100)] (unique),   // unused in complex queries
        3: first_name   [nvarchar(50)],             // unused in complex queries
        4: last_name    [nvarchar(50)],             // unused in complex queries
        5: gender       [nvarchar(1)],
        6: birth_date   [timestamp/integer]
    }
    
    t_location { // alias: l
        1: id           [uint32]        (unique),
        2: place        [nclob],
        3: country      [nvarchar(50)],
        4: city         [nvarchar(50)],             // unused in complex queries
        5: distance     [uint32]
    }
    
    t_visits { //   alias: v
        1: id           [uint32]        (unique),
        2: location     [uint32]        (fk),
        3: user         [uint32]        (fk),
        4: visited_at   [timestamp/integer],
        5: mark         [integer]       (0..5)
    } //user can visit location several times
    
    t_joint {
        1: v_id           [uint32]        (unique),
        2: v_location     [uint32]        (fk),
        3: v_user         [uint32]        (fk),
        4: v_visited_at   [timestamp/integer],
        5: v_mark         [integer]       (0..5)
        6: u_gender
        7: u_birthdate //u_age (=years(current_timestamp-u.birth_date))
        8: l_place
        9: l_country
       10: l_distance 
    }
# Logic
## GET /users/:id/visits
Alias: uv

Index: user_id(3), visited_at(4)

    SELECT
        v.mark,
        v.visited_at,
        l.place
    FROM
        visits v
        JOIN locations l
            ON v.location = l.id
    WHERE
        v.user = :id
        [ AND v.visited_at > fromDate ]
        [ AND v.visited_at < toDate ]
        [ AND l.country = country ]
        [ AND l.distance < toDistance ]

## GET /locations/:id/avg
Alias: la

    SELECT
        avg(v.mark)
    FROM
        visits v
        JOIN users u
            ON v.user = u.id
    WHERE
        v.location = :id
        [ AND v.visited_at > fromDate ]
        [ AND v.visited_at < toDate ]
        [ AND (current_timestamp-u.birth_date) > fromAge ]
        [ AND (current_timestamp-u.birth_date) < toAge ]
        [ AND u.gender = gender ]