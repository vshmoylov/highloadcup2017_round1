--
-- Created by IntelliJ IDEA.
-- User: inversed
-- Date: 18.08.2017
-- Time: 21:53
--

local L = {}

local function LOG(level, message)
    print(string.format('%s [%5s] %s',os.date('%Y-%m-%d %H:%M:%S'), level, message))
end

function L.info(message)
    LOG('INFO', message)
end

function L.warn(message)
    LOG('WARN', message)
end

function L.err(message)
    LOG('ERROR', message)
end

return L