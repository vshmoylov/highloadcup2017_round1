#!/usr/local/bin/tarantool

----[[
box.cfg{
    memtx_memory = 3500 * 1024 * 1024,
    wal_mode='none',
    checkpoint_interval = 0,
    checkpoint_count  = 1,
    log_level = 3,
}--]]


--require 'app.lib'
local log = require ('app.simplelog')
local jsoncodec = require('json')
local msgpack = require('msgpack')
--os.execute('pwd')
--printHello();

TIMESTAMP_NOW = os.time(); -- timestamp of a start
--DATE_NOW = os.date("*t", TIMESTAMP_NOW)
RUNNING_MODE = 0 -- "0" means test, "1" means prod
APP_VERSION = "1.0"

t_users = true
t_locations = true
t_visits = true
t_joint = true


local function initTables() -- All fields can not be null!
    log.info("Creating storage spaces & indexes")
    -- user { id[uint32](unique), email[nvarchar(100)](unique), first_name[nvarchar(50)], last_name[nvarchar(50)], gender[nvarchar(1)], birth_date[timestamp/integer] }
    t_users = box.schema.space.create('users')
    t_users:create_index('primary', {
        parts = {1, 'unsigned'},
        type = 'hash', -- MUST be unique
    })

    -- location { id[uint32](unique), place[nclob], country[nvarchar(50)], city[nvarchar(50)], distance[uint32] }
    t_locations = box.schema.space.create('locations')
    t_locations:create_index('primary', {
        parts = {1, 'unsigned'},
        type = 'hash',
    })

    -- visit { id[uint32](unique), location[uint32](fk), user[uint32](fk), visited_at[timestamp/integer], mark[integer](0..5) } //user can visit location several times
    t_visits = box.schema.space.create('visits')
    t_visits:create_index('primary', {
        parts = {1, 'unsigned'},
        type = 'hash',
    })

    t_joint = box.schema.space.create("joint")
    t_joint:create_index('primary', {
        parts = {1, 'unsigned'},
        type = 'hash'
    })
    t_joint:create_index('fk_loc', {
        parts = {2, 'unsigned'},
        type = 'tree',
        unique = false
    })
    t_joint:create_index('fk_usr', {
        parts = {3, 'unsigned'},
        type = 'tree',
        unique = false
    })
    t_joint:create_index('index_uv', {
        parts = {3, 'unsigned', 4, 'integer'},
        type = 'tree',
        unique = false
    })
    t_joint:create_index('index_la', {
        parts = {2, 'unsigned', 4, 'integer'},
        type = 'tree',
        unique = false
    })
end

--local function getAge(birth_date) --TODO rewrite
--    local DATE_BIRTH = os.date("*t", birth_date)
--    return (DATE_NOW.year - DATE_BIRTH.year) - (DATE_NOW.yday>DATE_BIRTH.yday and 0 or 1)
--    --return math.floor((TIMESTAMP_NOW - birth_date)/(365*24*60*60))
--end

local function ageToTimestamp(age)
    local dt = os.date("*t", TIMESTAMP_NOW)
    dt.year = dt.year - age
    return os.time(dt)
end

local function fillJointTable()
    log.info('Filling JOINT table')
    for _, rec_v in t_visits.index.primary:pairs(nil, {iterator = box.index.ALL}) do
        local rec_u = t_users:select(rec_v[3])[1]
        local rec_l = t_locations:select(rec_v[2])[1]
        t_joint:insert{
            rec_v[1],
            rec_v[2],
            rec_v[3],
            rec_v[4],
            rec_v[5],
            rec_u[5],
            rec_u[6], --getAge(rec_u[6])
            rec_l[2],
            rec_l[3],
            rec_l[5]
        }
    end
end

local function readAll(file)
    local f = io.open(file, "rb")
    local content = f:read("*all")
    f:close()
    return content
end



local function parseJson(jsonString)
    local valid, result = pcall(jsoncodec.decode, jsonString)
    if not valid then result = nil end
    return result
end

local function readOptions(filename)
    local f = io.open(filename, 'r')
    for i = 1,2 do
        if 1 == i then --noinspection GlobalCreationOutsideO
            TIMESTAMP_NOW = tonumber(f:read())
--            DATE_NOW = os.date("*t", TIMESTAMP_NOW)
        else --noinspection GlobalCreationOutsideO
            RUNNING_MODE = tonumber(f:read()) end
    end
    f:close()
    log.info("Timestamp from options: "..TIMESTAMP_NOW)
    log.info("Running mode: "..RUNNING_MODE)
end

local function insertLocations(list)
--    log.info("Locations HANDLER")
--    log.info("inserting "..#list..' records')
    for i = 1, #list do
--        print(jsoncodec.encode(list[i]))
        local loc = list[i]
        t_locations:insert{loc.id,loc.place, loc.country, loc.city, loc.distance}
    end
    return true
end
local function insertUsers(list)
--    log.info("Users HANDLER")
--    log.info("inserting "..#list..' records')
    for i = 1, #list do
        local usr = list[i]
        t_users:insert{usr.id, usr.email, usr.first_name, usr.last_name, usr.gender, usr.birth_date}
    end
    return true
end
local function insertVisits(list)
--    log.info("Users HANDLER")
--    log.info("inserting "..#list..' records')
    for i = 1, #list do
        local visit = list[i]
        t_visits:insert{visit.id, visit.location, visit.user, visit.visited_at, visit.mark}
    end
    return true
end


local dataLoadHandlerTable = { -- thx to http://lua-users.org/wiki/SwitchStatement
    locations = insertLocations,
    users     = insertUsers,
    visits    = insertVisits
}

local function insertData(array)
    for key, val in pairs(array) do  -- Table iteration.
--        log.info("Inserting data for "..key)
        local handler = dataLoadHandlerTable[key];
        local _ = type(handler)=='function' and handler(val) or log.warn("No loader defined for "..key)
    end
end



--unzip data: unzip -o -d /opt/tarantool/ /tmp/data/data.zip
log.info("App version: "..APP_VERSION)
log.info("Unzipping")
os.execute('unzip -o -d /opt/tarantool/initial/ /tmp/data/data.zip > /dev/null')

initTables();

log.info("Current timestamp is:   "..TIMESTAMP_NOW)
log.info("Reading options.txt")
readOptions("/tmp/data/options.txt")
log.info("Processing initial data")
local filez = io.popen('ls -1 /opt/tarantool/initial/')
local fname = filez:read()
while fname do
    local fullfname = '/opt/tarantool/initial/'..fname
    local status = 'Ok'
    if fname == 'options.txt' then
--        readOptions(fullfname)
--        log.info("Timestamp from options: "..TIMESTAMP_NOW)
--        log.info("Running mode: "..RUNNING_MODE)
    else
        local contents = readAll(fullfname)
        local objs = parseJson(contents)
        contents = nil
        if objs then insertData(objs) --insert data
        else status = 'invalid' end
    end

--    log.info("FILE "..fname..': '..status)

    fname = filez:read() -- goto next file
end
filez:close()

fillJointTable()


--[[


# Entities:
    user
    location
    visit

Need to handle following methods:

* GET /<entity>/<id> - 200|404
    * GET /users/:id
    * GET /locations/:id
    * GET /visits/:id

* GET /users/<id>/visits - 200|400|404
    {"visits": [ ... ]} ordered by visited_at ASC
    Parameters:
        fromDate - visited_at > fromDate
        toDate   - visited_at < toDate
        country
        toDistance - distance < toDistance

* GET /locations/<id>/avg - 200|400|404
    {"avg": 0.12345}
    Parameters:
        fromDate - visited_at > fromDate
        toDate   - visited_at < toDate
        fromAge  - (current_timestamp-birth_date) > fromAge
        toAge    - (current_timestamp-birth_date) < toAge
        gender

* POST /<entity>/<id> - 200|400|404
    Updates existing entity

* POST /<entity>/new  - 200|400
    Creates new entity
]]

local function respondError(code)
    return { status = code }
end

local function respondEmptyJsonObject()
    return {
        status = 200,
        headers = {
            ['content-type'] = 'application/json; charset=utf-8'
        },
        body='{}'
    }
end

local entityTables = {
    users = t_users,
    locations = t_locations,
    visits = t_visits
}

local entityJsonMappers = {
    users = function (data)
        return {
            id = data[1],
            email = data[2],
            first_name = data[3],
            last_name = data[4],
            gender = data[5],
            birth_date = data[6]
        }
    end,
    locations = function (data)
        return {
            id = data[1],
            place = data[2],
            country = data[3],
            city = data[4],
            distance = data[5]
        }
    end,
    visits = function (data)
        return {
            id = data[1],
            location = data[2],
            user = data[3],
            visited_at = data[4],
            mark = data[5]
        }
    end
}

local function getEntityByIdHandler(self) -- GET /<entity>/<id>
    local method = self.method -- need to handle GET and POST
    local entity = self:stash('entity')
    local route_id = self:stash('id')
--    log.info(jsoncodec.encode(reqInfo))
    if method == 'GET' then
        local lkup_table = entityTables[entity]
        local mapper = entityJsonMappers[entity]
        local id = tonumber64(route_id)
        if id and lkup_table then
            local found = lkup_table:select(id)[1]
            if found then return self:render{json = mapper(found)} end
        end
        return respondError(404)
    elseif method == 'POST' then
        -- TODO handle entities updates
    end
end

local function getUserVisits(self) -- GET /users/<id>/visits
    local route_id = self:stash('id')
    local id = tonumber64(route_id)
    if not id then return respondError(404) end
    local user = t_users:select(id)[1]
    if not user then return respondError(404) end

    local fromDate
    local toDate
    local country
    local toDistance

    local param
    param = self:query_param('fromDate')
    if param then
        fromDate = tonumber64(param)
        if not fromDate then return respondError(400) end
    end
    param = self:query_param('toDate')
    if param then
        toDate = tonumber64(param)
        if not toDate then return respondError(400) end
    end
    param = self:query_param('country')
    if param then
        country = param
    end
    param = self:query_param('toDistance')
    if param then
        toDistance = tonumber64(param)
        if not toDistance then return respondError(400) end
    end

    local resultset = {}

    for _, tuple in t_joint.index.index_uv:pairs({id, fromDate} , {iterator = box.index.GE}) do
        if tuple[3] ~= id then break end
        if toDate and tuple[4] >= toDate then break end
        if not country or tuple[9] == country then
            if not toDistance or tuple[10]<toDistance then
                table.insert(resultset, {
                    mark = tuple[5],
                    visited_at = tuple[4],
                    place = tuple[8]
                })
            end
        end
    end

--    if #resultset == 0 then return respondError(404) end

    return self:render{json = {visits = resultset}}

end

local function round(num, numDecimalPlaces)
    local mult = 10^(numDecimalPlaces or 0)
    return math.floor(num * mult + 0.5) / mult
end

local function getLocationAverage(self) -- GET /locations/<id>/avg
    local route_id = self:stash('id')
    local id = tonumber64(route_id)
    if not id then return respondError(404) end
    local location = t_locations:select(id)[1]
    if not location then return respondError(404) end

    local fromDate
    local toDate
    local fromAge
    local toAge
    local fromTs
    local toTs
    local gender

    local param
    param = self:query_param('fromDate')
    if param then
        fromDate = tonumber64(param)
        if not fromDate then return respondError(400) end
    end
    param = self:query_param('toDate')
    if param then
        toDate = tonumber64(param)
        if not toDate then return respondError(400) end
    end
    param = self:query_param('fromAge')
    if param then
        fromAge = tonumber64(param)
        if not fromAge then return respondError(400) end
        toTs = ageToTimestamp(fromAge)
    end
    param = self:query_param('toAge')
    if param then
        toAge = tonumber64(param)
        if not toAge then return respondError(400) end
        fromTs = ageToTimestamp(toAge)
    end
    param = self:query_param('gender')
    if param then
        gender = param
        if gender ~= 'f' and gender ~= 'm' then return respondError(400) end
    end

    local rating_sum = 0
    local rating_counted = 0



    for _, tuple in t_joint.index.index_la:pairs({id, fromDate} , {iterator = box.index.GE}) do
        if tuple[2] ~= id then break end
        if toDate and tuple[4] >= toDate then break end
        if not fromTs or tuple[7] > fromTs then
            if not toTs or tuple[7] < toTs then
                if not gender or tuple[6] == gender then
                    rating_sum = rating_sum + tuple[5]
                    rating_counted = rating_counted + 1
                end
            end
        end
    end

    local avg = 0.0
    if rating_counted ~= 0 then avg = round(rating_sum/rating_counted,5) end

    return self:render{json = {avg = avg}}


end

local function postNewUser(req)
    --local user = req:json()
    local valid, user = pcall(req.json, req)
    if not valid then return respondError(400) end
    
    user.id = tonumber64(user.id)
    user.birth_date = tonumber(user.birth_date)
    if not user.id then return respondError(400) end
    if jsoncodec.NULL == user.email then return respondError(400) end
    if jsoncodec.NULL == user.first_name then return respondError(400) end
    if jsoncodec.NULL == user.last_name then return respondError(400) end
    if user.gender ~= 'f' and user.gender ~= 'm' then return respondError(400) end
    if not user.birth_date then return respondError(400) end
    
    local valid, result = pcall(t_users.insert, t_users, {user.id, user.email, user.first_name, user.last_name, user.gender, user.birth_date})
    
    if not valid then return respondError(400)
    else return respondEmptyJsonObject() end 
end

local function postNewLocation(req)
    --local loc = req:json()
    local valid, loc = pcall(req.json, req)
    if not valid then return respondError(400) end
    
    loc.id = tonumber64(loc.id)
    loc.distance = tonumber64(loc.distance)
    if not loc.id then return respondError(400) end
    if jsoncodec.NULL == loc.place then return respondError(400) end
    if jsoncodec.NULL == loc.country then return respondError(400) end
    if jsoncodec.NULL == loc.city then return respondError(400) end
    if not loc.distance then return respondError(400) end
    
    local valid, result = pcall(t_locations.insert, t_locations, {
        loc.id,
        loc.place,
        loc.country,
        loc.city,
        loc.distance
    })
    
    if not valid then return respondError(400)
    else return respondEmptyJsonObject() end 
end

local function postNewVisit(req)
    --local visit = req:json()
    local valid, visit = pcall(req.json, req)
    if not valid then return respondError(400) end
    
    visit.id = tonumber64(visit.id)
    visit.location = tonumber64(visit.location)
    visit.user = tonumber64(visit.user)
    visit.visited_at = tonumber(visit.visited_at)
    visit.mark = tonumber64(visit.mark)
    if  not visit.id or
        not visit.location or
        not visit.user or
        not visit.visited_at or
        not visit.mark
    then return respondError(400) end
    local user = t_users:select(visit.user)[1]
    local location = t_locations:select(visit.location)[1]
    if not user or not location then return respondError(400) end
    
    local valid, result = pcall(t_visits.insert, t_visits, {
        visit.id,
        visit.location,
        visit.user,
        visit.visited_at,
        visit.mark
    })
    
    if not valid then return respondError(400) end
    
    t_joint:insert{
        visit.id,
        visit.location,
        visit.user,
        visit.visited_at,
        visit.mark,
        user[5],
        user[6],--getAge(user[6])
        location[2],
        location[3],
        location[5]
    }
    return respondEmptyJsonObject()
end

local function updateUser(req)
    local route_id = req:stash('id')
    local id = tonumber64(route_id)
    if not id then return respondError(404) end
    
    
    local valid, user = pcall(req.json, req)
    if not valid then return respondError(400) end

    local toUpdate = {}
    local toUpdateJoint = {}
    if type(user.email) ~= 'nil' then
        if jsoncodec.NULL == user.email then return respondError(400)
        else table.insert(toUpdate, {'=', 2, user.email}) end
    end
    if type(user.first_name) ~= 'nil' then
        if jsoncodec.NULL == user.first_name then return respondError(400)
        else table.insert(toUpdate, {'=', 3, user.first_name}) end
    end
    if type(user.last_name) ~= 'nil' then
        if jsoncodec.NULL == user.last_name then return respondError(400)
        else table.insert(toUpdate, {'=', 4, user.last_name}) end
    end
    if type(user.gender) ~= 'nil' then
        if user.gender ~= 'f' and user.gender ~= 'm' then return respondError(400)
        else
            table.insert(toUpdate, {'=', 5, user.gender})
            table.insert(toUpdateJoint, {'=', 6, user.gender})
        end
    end
    if type(user.birth_date) ~= 'nil' then
        user.birth_date = tonumber(user.birth_date)
        if not user.birth_date then return respondError(400)
        else
            table.insert(toUpdate, {'=', 6, user.birth_date})
--            table.insert(toUpdateJoint, {'=', 7, getAge(user.birth_date)})
            table.insert(toUpdateJoint, {'=', 7, user.birth_date})
        end
    end

    
    local record = t_users:select(id)[1]
    if not record then return respondError(404) end
    
    t_users:update(id, toUpdate)
    
    
    local ids = {}
    for _, tuple in t_joint.index.fk_usr:pairs({id} , {iterator = box.index.EQ}) do
        table.insert(ids, tuple[1])
    end
    
    for i = 1, #ids do
        t_joint:update(ids[i], toUpdateJoint)
    end
    
    return respondEmptyJsonObject()
end

local function updateLocation(req)
    local route_id = req:stash('id')
    local id = tonumber64(route_id)
    if not id then return respondError(404) end
    
    local record = t_locations:select(id)[1]
    if not record then return respondError(404) end

    local valid, loc = pcall(req.json, req)
    if not valid then return respondError(400) end

    local toUpdate = {}
    local toUpdateJoint = {}

    if type(loc.place) ~= 'nil' then
        if jsoncodec.NULL == loc.place then return respondError(400)
        else
            table.insert(toUpdate, {'=', 2,loc.place})
            table.insert(toUpdateJoint, {'=', 8, loc.place})
        end
    end
    if type(loc.country) ~= 'nil' then
        if jsoncodec.NULL == loc.country then return respondError(400)
        else
            table.insert(toUpdate,      {'=', 3, loc.country})
            table.insert(toUpdateJoint, {'=', 9, loc.country})
        end
    end

    if type(loc.city) ~= 'nil' then
        if jsoncodec.NULL == loc.city then return respondError(400)
        else table.insert(toUpdate, {'=', 4, loc.city}) end
    end
    if type(loc.distance) ~= 'nil' then
        loc.distance = tonumber64(loc.distance)
        if not loc.distance then return respondError(400)
        else
            table.insert(toUpdate, {'=', 5, loc.distance})
            table.insert(toUpdateJoint, {'=', 10, loc.distance})
        end
    end

    t_locations:update(id, toUpdate)

    local ids = {}
    for _, tuple in t_joint.index.fk_loc:pairs({id} , {iterator = box.index.EQ}) do
        table.insert(ids, tuple[1])
    end

    for i = 1, #ids do
        t_joint:update(ids[i], toUpdateJoint)
    end
    return respondEmptyJsonObject()
end

local function updateVisit(req)
    local route_id = req:stash('id')
    local id = tonumber64(route_id)
    if not id then return respondError(404) end
    
    local record = t_visits:select(id)[1]
    if not record then return respondError(404) end
    
    local valid, visit = pcall(req.json, req)
    if not valid then return respondError(400) end


    local toUpdate = {}

    if type(visit.location) ~= 'nil' then
        visit.location = tonumber64(visit.location)
        if not visit.location then return respondError(400)
        else table.insert(toUpdate, {'=', 2, visit.location}) end
    end
    if type(visit.user) ~= 'nil' then
        visit.user = tonumber64(visit.user)
        if not visit.user then return respondError(400)
        else table.insert(toUpdate, {'=', 3, visit.user}) end
    end
    if type(visit.visited_at) ~= 'nil' then
        visit.visited_at = tonumber(visit.visited_at)
        if not visit.visited_at then return respondError(400)
        else table.insert(toUpdate, {'=', 4, visit.visited_at}) end
    end
    if type(visit.mark) ~= 'nil' then
        visit.mark = tonumber64(visit.mark)
        if not visit.mark then return respondError(400)
        else table.insert(toUpdate, {'=', 5, visit.mark}) end
    end

    
    t_visits:update(id, toUpdate)
    t_joint:update(id, toUpdate)
    
    return respondEmptyJsonObject()
end

log.info("Starting web server")
local httpd = require('http.server')
local server = httpd.new('0.0.0.0', 80)
server:route({path = '/users/new', method = 'POST'}, postNewUser)
server:route({path = '/users/:id', method = 'POST'}, updateUser)
server:route({path = '/locations/new', method = 'POST'}, postNewLocation)
server:route({path = '/locations/:id', method = 'POST'}, updateLocation)
server:route({path = '/visits/new', method = 'POST'}, postNewVisit)
server:route({path = '/visits/:id', method = 'POST'}, updateVisit)
server:route({path = '/users/:id/visits'}, getUserVisits)
server:route({path = '/locations/:id/avg'}, getLocationAverage)
server:route({path = '/:entity/:id', method = 'GET'}, getEntityByIdHandler)
server:start();
