#!/bin/bash
docker run --name taran -p80:80 --rm -it --net host\
    -v `pwd`/app:/opt/tarantool/app \
    -v `pwd`/data.zip:/tmp/data/data.zip \
    -v `pwd`/options.txt:/tmp/data/options.txt \
    -v `pwd`/config.yml:/etc/tarantool/config.yml \
    mytaran tarantool /opt/tarantool/app/app.lua