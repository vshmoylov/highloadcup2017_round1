FROM mytaran

ADD config.yml /etc/tarantool/config.yml

ADD app /opt/tarantool/app

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 80

CMD ["tarantool", "/opt/tarantool/app/app.lua"]